Proyecto del curso “Learn JavaScript from Scratch: The Ultimate Beginners Course” (UDEMY)
Proyecto centrado en los fundamentos de HTML, CSS y JavaScript.

El objetivo de este ejercicio fue realizar una aplicación simple para gestionar el control de ingresos y egresos, con sus respectivos 
detalles y valores, y así también calcular el presupuesto final.
El usuario puede modificar los datos.
Fue el primer ejercicio realizado con JavaScript.

La idea principal fue trabajar más el concepto la programación con JavaScript sobre el DOM HTML.

La creación de objetos de tipo Ingreso y Egreso extienden de la clase Dato, de esta manera se trabajó también el concepto de herencia
en objetos.
Por medio de la clase App, se realiza la interacción con la página HTML, operaciones de tipo cargar egresos, modificarlos y eliminarlos, así
como también listarlos al terminar de cargarse la página (actualizando los arreglos locales de Ingresos y Egresos).


